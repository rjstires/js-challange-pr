## Code Review
The provided solution was hard to test, hard to read, and hard to extend.

## Readabilty
Readability is subjective and the code sample was provided by email (copied and pasted into the
email body) so it's difficult to judge readability. Given enough time a junior developer could read
through and understand what the result would be given a set of parameters.

## Maintainability
The provided solution would not be easily maintained. We had a limited number of parameters, each
requiring the same pattern. If the value is true; invoke a given function and continue considering
branches. If the value is false; invoke a function, return
false, and no longer consider further branches.

## Simplicity
Simplicity is also subjective. Again the solution used basic decision making to arrive at a
conclusion (true/value) and potentially invoke a function.

## Testability
Although not asked, this is a significant concern. I've found consistently that hard to test code
is hard to maintain, hard to explain, and hard to refactor when the day comes. Testing provides
baked-in documentation and guards against regression in the future.

My proposed solution changes the API to take N number of functions. Each function is an individual
testable unit. The pattern is fairly straight-forward. It consist of a function to invoke for
success, a function to invoke for failure, and the value to check. These functions are reduced to
the boolean value. The decision tree is halted based on the first false value encountered.
