const ifElse = (ifFn, elseFn, value) => () => {
  if (value) {
    ifFn();
    return true;
  } else {
    elseFn();
    return false;
  }
};

const chainSuccess = (...fns) => fns.reduce((result, fn) => result ? fn() : result, true);

module.exports = {
  ifElse,
  chainSuccess,
}
