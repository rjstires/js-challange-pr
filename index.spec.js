const { chainSuccess, ifElse } = require('./index');

describe('chainSuccess', () => {
  const aSucess = jest.fn();
  const aFailure = jest.fn();
  const bSucess = jest.fn();
  const bFailure = jest.fn();
  const cSuccess = jest.fn();
  const cFailure = jest.fn();

  beforeEach(() => {
    aSucess.mockReset();
    aFailure.mockReset();
    bSucess.mockReset();
    bFailure.mockReset();
    cSuccess.mockReset();
    cFailure.mockReset();
  });

  it('should return true if all values are truthy', () => {
    const result = chainSuccess(
      ifElse(aSucess, aFailure, true),
      ifElse(bSucess, bFailure, true),
      ifElse(cSuccess, cFailure, true),
    );

    expect(result).toBeTruthy();
  });

  it('should return false if any value is false', () => {
    const result = chainSuccess(
      ifElse(aSucess, aFailure, true),
      ifElse(bSucess, bFailure, false),
      ifElse(cSuccess, cFailure, true),
    );

    expect(result).toBeFalsy();
  });

  it('should invoke success functions until a falsy value is encounted', () => {
    chainSuccess(
      ifElse(aSucess, aFailure, true),
      ifElse(bSucess, bFailure, true),
      ifElse(cSuccess, cFailure, true),
    );

    expect(aSucess).toBeCalled();
    expect(aFailure).not.toBeCalled();

    expect(bSucess).toBeCalled();
    expect(bFailure).not.toBeCalled();

    expect(cSuccess).toBeCalled();
    expect(cFailure).not.toBeCalled();
  });

  it('should invoke the failure function when the first falsy value is encountered', () => {
    chainSuccess(
      ifElse(aSucess, aFailure, true),
      ifElse(bSucess, bFailure, false),
    );

    expect(aSucess).toBeCalled();
    expect(aFailure).not.toBeCalled();

    expect(bSucess).not.toBeCalled();
    expect(bFailure).toBeCalled();
  });

  it('should should not invoke any functions after the first falsy value is encountered', () => {
    chainSuccess(
      ifElse(aSucess, aFailure, true),
      ifElse(bSucess, bFailure, false),
      ifElse(cSuccess, cFailure, true),
    );

    expect(aSucess).toBeCalled();
    expect(aFailure).not.toBeCalled();

    expect(bSucess).not.toBeCalled();
    expect(bFailure).toBeCalled();

    expect(cSuccess).not.toBeCalled();
    expect(cFailure).not.toBeCalled();
  });
});
